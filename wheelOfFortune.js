$(document).ready(function() {
   
    var oldVal;
    $(document).on('change paste textInput input', ".item-input", function (e) {
        var val = this.value;
        if (val !== oldVal) {
            oldVal = val;
            var inputs = $(".item-input");
            if(val != "" && inputs[inputs.length - 1] === this) {
                $('<div class="input-wrapper"><input class="item-input" type="text"><span class="remove-input" style="display:none;">X</span></div>').insertAfter($(this.parentNode));
                if (this !== inputs[0]) {
                    $(this).siblings('.remove-input').show();
                }
            }
        }
    });

    $(document).on('click', ".remove-input", function (e) {
        var inputs = $(".item-input");
        if (inputs[inputs.length - 1] === this)
            this.value = "";
        else {
            $(this.parentNode).remove();            
        }
    });

    var side = Math.min(getViewportHeight(), getViewportWidth()) / 2;
    $('#wheel').attr('width', side).attr('height', side);
    showItems();
});

function applyClicked() {
    
    var elements = $('.item-input'),
        validItems = [];
    
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].value !== "")
            validItems.push(elements[i].value);
    }

    $('#items-box').animate({top: '100%'}, {duration: 300})
        .siblings().fadeIn(200);
    
    clearWheel();
    drawWheel(validItems);
    initializeWheel(validItems);
}

function clearWheel() {
    $('#wheel').empty();
}

function drawWheel(items) {
    
    var margin = 5,
        wheel = $('#wheel'),
        radius = Math.min(wheel.width(), wheel.height()) / 2 - margin * 2,
        angleStep = Math.PI * 2 / items.length,
        curAngle = -angleStep / 2,
        
        colors = ['rgb(255, 255, 0)', 'rgb(0, 214, 255)', 'rgb(255, 92, 0)', 'rgb(188, 68, 255)', 'rgb(255, 143, 0)', 'rgb(255, 68, 233)', 'rgb(255, 204, 0)', 'rgb(255, 0, 0)', 'rgb(224, 255, 0)', 'rgb(163, 255, 0)', 
            'rgb(0, 255, 224)', 'rgb(0, 163, 255)', 'rgb(225, 68, 255)', 'rgb(0, 255, 61)', 'rgb(255, 73, 88)'],

        spinButton = document.createElementNS('http://www.w3.org/2000/svg', 'circle'),
        pointer = document.createElementNS('http://www.w3.org/2000/svg', 'polygon'),
        staticGroup = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
        spinWrapper = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
        spinGroup = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        
    spinButton.setAttributeNS(null, 'cx', 0);
    spinButton.setAttributeNS(null, 'cy', 0);
    spinButton.setAttributeNS(null, 'r', radius / 6);
    spinButton.setAttributeNS(null, 'fill', 'blue');

    pointer.setAttributeNS(null, 'points', -radius / 12 + ',' + -radius / 24 + ' ' + radius / 12 + ',' + -radius / 24 + ' 0,' + -radius / 2);
    pointer.setAttributeNS(null, 'fill', 'blue');

    spinGroup.setAttribute('id', 'spinGroup');
    staticGroup.setAttribute('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')');
    staticGroup.appendChild(spinButton);
    staticGroup.appendChild(pointer);

    spinWrapper.setAttribute('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')');
    spinWrapper.appendChild(spinGroup);

    for (var i = 0; i < items.length; i++) {
        var g = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
            path = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
            text = document.createElementNS('http://www.w3.org/2000/svg', 'text'),
            textPath = document.createElementNS('http://www.w3.org/2000/svg', 'textPath'),
            pathForText = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
            defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs'),
            use = document.createElementNS(null, 'use'),
            
            start = {
                x: Math.sin(curAngle) * radius,
                y: Math.cos(curAngle) * radius
            },
            middle = {
                x: Math.sin(curAngle + angleStep / 2) * radius,
                y: Math.cos(curAngle + angleStep / 2) * radius
            },
            end = {
                x: Math.sin(curAngle + angleStep) * radius,
                y: Math.cos(curAngle + angleStep) * radius
            };
        
        path.setAttribute('d', 'M' + start.x + ',' + -start.y + 'A' + radius + ',' + radius + ' 0 0,1 ' + end.x + ',' + -end.y + 'L0,0Z');
        path.setAttribute('fill', shadeRGBColor(colors[i], -0.15));
        path.setAttribute('stroke', colors[i]);
        path.setAttribute('stroke-width', 4);

        pathForText.setAttribute('d', 'M' + 0 + ',' + 0 + 'L' + middle.x + ',' + -middle.y + 'Z');
        pathForText.id = 'textPath' + colors[i];
        defs.appendChild(pathForText);
        
        use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#textPath' + colors[i]);
        
        text.setAttributeNS(null, 'dx', radius - 5);
        text.setAttributeNS(null, 'dy', '5');
        text.setAttributeNS(null, 'text-anchor', 'end');
        text.appendChild(textPath);
        textPath.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#textPath' + colors[i]);
        textPath.appendChild(document.createTextNode(items[i]));
        
        curAngle += angleStep;
        
        g.appendChild(path);
        g.appendChild(defs);
        g.appendChild(use);
        g.appendChild(text);
        spinGroup.appendChild(g);
    }

    wheel.append(spinWrapper);
    wheel.append(staticGroup);
}

function showItems() {

    $('#items-box').animate({top: '50%'}, {duration: 300})
        .siblings().fadeOut(200);
}

function initializeWheel(items) {

    var wheel = $('#spinGroup').fortune(items.length);

    wheel.fortune(items);

    wheel.fortune({
      prices: items,
      duration: 3000, // The amount of milliseconds the roulette to spin
      separation: 0, // The separation between each roulette price
      min_spins: 10, // The minimum number of spins 
      max_spins: 15, // The maximum number of spins
      onSpinBounce: function() {
        console.log('onSpinBounce');
        //Sounds.play('taka');
      } // A callback to be called each time the roulette hits a price bound.
    })

    wheel.click(function() {
        $('#result').hide()
        wheel.spin().then(function(result) {
            $('#result').show().html(result);
            
        });;
    });
/*
    // The spin methods returns a promise, which its first arguments is the object that
    // is at that position (only if you have used the second form of initilization)
    wheel.spin().then(function(price) {
      console.log(price.description);
    });

    // or you can specify a fixed price
    wheel.spin(1).then(function(price) {
      console.log(price.description); // "200 u$s"
    });*/
}

function shadeRGBColor(color, percent) {
    var f=color.split(","),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);
    return "rgb("+(Math.round((t-R)*p)+R)+","+(Math.round((t-G)*p)+G)+","+(Math.round((t-B)*p)+B)+")";
}

function getViewportHeight() {
    return window.innerHeight ||
           document.documentElement.clientHeight ||
           document.body.clientHeight;
}

function getViewportWidth() {
    return window.innerWidth ||
           document.documentElement.clientWidth ||
           document.body.clientWidth;
}

function getSVGTextSize(textStr, fontSize, fontFamily) {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
        text = document.createElementNS("http://www.w3.org/2000/svg", "text"),
        gr = document.createElementNS("http://www.w3.org/2000/svg", "g"),
        gr2 = document.createElementNS("http://www.w3.org/2000/svg", "g");

    $(text).append(textStr);
    $(svg).css("opacity", 0);
    $(svg).css("width", 50);
    $(svg).css("height", 50);
    $(text).css("font-size", fontSize + "px");
    $(text).css("font-family", fontFamily || ChartSettings.fontFamily);
    $(gr).append(gr2);
    $(gr2).append(text);
    $(svg).append(gr);
    document.body.appendChild(svg);
    var bbox = text.getBBox();
    document.body.scrollTop = 50;
    $(svg).remove();
    return bbox;
}
